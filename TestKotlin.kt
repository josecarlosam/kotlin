
class TestKotlin {
    fun message() {
        println("Welcome to Test of Kotlin language!")
    }
}

fun main() {
    val testKotlin = TestKotlin()

    testKotlin.message()
}